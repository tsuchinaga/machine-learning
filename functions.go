package machine_learning

// EF - 誤差関数(error function)
type EF func(out, answer float64) float64

func SimpleErrorFunction(out, answer float64) float64 {
	return answer - out
}

// AF - 活性化関数(activation function)
type AF func(out float64) float64

func SimpleActivationFunction(out float64) float64 {
	if out > 0 {
		return 1
	} else {
		return 0
	}
}
