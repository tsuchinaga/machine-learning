package machine_learning

import "errors"

var (
	inputCountNotMatchErr  = errors.New("input count does not match")
	answerCountNotMatchErr = errors.New("answer count does not match")
)
