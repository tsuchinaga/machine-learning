package machine_learning

import "math/rand"

func NewSimplePerceptron(in int, out int, bias float64, learningRatio float64, seed int64) *SimplePerceptron {
	r := rand.New(rand.NewSource(seed))

	weights := make([][]float64, in+1) // バイアス項を追加
	for i := range weights {
		weights[i] = make([]float64, out)
		for j := 0; j < out; j++ {
			weights[i][j] = r.Float64()
		}
	}

	return &SimplePerceptron{
		in:            in,
		out:           out,
		bias:          bias,
		weights:       weights,
		learningRatio: learningRatio,
		EF:            SimpleErrorFunction,
		AF:            SimpleActivationFunction,
	}
}

type SimplePerceptron struct {
	in            int         // 入力ノード数
	out           int         // 出力ノード数
	bias          float64     // バイアス
	weights       [][]float64 // 重みづけ 入力nと出力mの間の重み
	learningRatio float64     // 学習率
	EF            EF          // 誤差関数
	AF            AF          // 活性化関数
}

func (sp *SimplePerceptron) Output(ins []float64) ([]float64, error) {
	if len(ins) != sp.in {
		return nil, inputCountNotMatchErr
	}

	outs := make([]float64, sp.out)
	inputs := append(ins, sp.bias) // バイアス項に与えるデータを追加
	for j := 0; j < sp.out; j++ {
		for i := range sp.weights {
			outs[j] += inputs[i] * sp.weights[i][j]
		}
	}

	for j := range outs {
		outs[j] = sp.AF(outs[j])
	}

	return outs, nil
}

func (sp *SimplePerceptron) Train(ins []float64, answers []float64) error {
	if len(ins) != sp.in {
		return inputCountNotMatchErr
	}
	if len(answers) != sp.out {
		return answerCountNotMatchErr
	}

	// spの回答を取得
	outs, err := sp.Output(ins)
	if err != nil {
		return err
	}

	inputs := append(ins, sp.bias) // バイアス項に与えるデータを追加
	for j, out := range outs {
		d := sp.EF(out, answers[j])
		for i := range sp.weights {
			sp.weights[i][j] = sp.weights[i][j] + inputs[i]*d*sp.learningRatio
		}
	}

	return nil
}
