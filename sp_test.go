package machine_learning

import (
	"reflect"
	"testing"
	"time"
)

func Test_SimplePerceptron_Train(t *testing.T) {
	t.Parallel()

	type train struct {
		inputs []float64
		answer []float64
	}

	// AND, OR, NAND, NORを学習
	trains := []*train{
		{inputs: []float64{0, 0}, answer: []float64{0, 0, 1, 1}},
		{inputs: []float64{0, 1}, answer: []float64{0, 1, 1, 0}},
		{inputs: []float64{1, 0}, answer: []float64{0, 1, 1, 0}},
		{inputs: []float64{1, 1}, answer: []float64{1, 1, 0, 0}},
	}

	// 学習
	sp := NewSimplePerceptron(2, 4, 1, 0.2, time.Now().Unix())
	for i := 0; i < 100; i++ {
		for _, train := range trains {
			if err := sp.Train(train.inputs, train.answer); err != nil {
				t.Errorf("%s error\n%+v\n", t.Name(), err)
			}
		}
	}

	// 検証
	for _, train := range trains {
		outs, err := sp.Output(train.inputs)
		if err != nil {
			t.Errorf("%s error\n%+v\n", t.Name(), err)
		}

		for i, out := range outs {
			if !reflect.DeepEqual(train.answer[i], out) {
				t.Errorf("%s failed\nwant: %+v\ngot: %+v\n", t.Name(), trains[i].answer, out)
			}
		}
	}
}
